<?php

class UserModel
{

	public function __construct()
	{
		require_once(__DIR__ . "/../config/Database.php");
		$this->db = new Database();
	}

	public function getAll()
	{
		return $this->db->query("SELECT * FROM USERS");
	}

	public function find($id)
	{
		return $this->db->query("SELECT * FROM USERS WHERE id = " . $id);
	}

	public function store($data)
	{
		return $this->db->query("INSERT INTO USERS (name, phone) VALUES ($data['name'], $data['gender'])");
	}

	public function update($id, $data)
	{

	}

	public function delete($id)
	{

	}
}