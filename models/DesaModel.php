<?php

class model_desa
{	

	protected $conn;

	public function __construct()
	{
		require_once (__DIR__ . '/../config/Database.php');
		$this->conn = (new koneksi())->connect();
	}

	public function add_desa($id_desa, $nama_desa)
	{

		$stmt = $this->conn->prepare ("INSERT INTO `desa` (`id_desa`, `nama_desa`)
        VALUES (?, ?)") or die($this->conn->error);
        //mengikuti character sesuai database
		$stmt->bind_param("is", $id_desa, $nama_desa);
		if($stmt->execute())
		{
            $stmt->close();
			$this->conn->close();
			return true;
		}
    }
    
	public function read_desa()
	{
		$stmt = $this->conn->prepare("SELECT * FROM `desa` ORDER BY `id_desa` ASC") or die($this->conn->error);
		if($stmt->execute()){
			$result = $stmt->get_result();
			return $result;
		}
	}


	public function hapus($id_desa)
	{
			$con = $this->conn;
			mysqli_query($con ,"delete from desa where id_desa = '$id_desa'");
		}

	//Fungsi untuk edit menampilkan data yang dipilih 
	public function read_edit_desa($id_desa){
		$con = $this->conn;
		$result = mysqli_query($con ,"select * from desa where id_desa = '$id_desa'");
		return $result;
	}

	public function edit_desa($id_desa, $nama_desa){
		$con = $this->conn;
		$result = mysqli_query($con,"update desa set nama_desa='$nama_desa'");
		return $result;
	}

	public function edit_desa2($id_desa, $nama_desa){
		$con = $this->conn;
		$result = mysqli_query($con,"update desa set id_desa='$id_desa', nama_desa='$nama_desa'");
		return $result;
	}
}