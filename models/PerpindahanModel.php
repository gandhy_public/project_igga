<?php

class model_perpindahan
{	

	protected $conn;

	public function __construct()
	{
		require_once (__DIR__ . '/../config/Database.php');
		$this->conn = (new koneksi())->connect();
	}

	public function add_perpindahan($data_penduduk_nik, $data_penduduk_id_data_penduduk, $alasan_pindah, $alamat_pindah, $tanggal_pindah)
	{
		$sql = "INSERT INTO `data_pindah` (`data_penduduk_nik`, `data_penduduk_id_data_penduduk`, `alasan_pindah`, `alamat_pindah`, `tanggal_pindah`) VALUES ('$data_penduduk_nik', '$data_penduduk_id_data_penduduk', '$alasan_pindah', '$alamat_pindah', '$tanggal_pindah')";

		if ($this->conn->query($sql) === TRUE) {
		    $delete = $this->conn->prepare("DELETE FROM `data_penduduk_kartu_keluarga` WHERE data_penduduk_id_data_penduduk='$data_penduduk_id_data_penduduk'") or die($this->conn->error);
			$delete->execute();

			$this->conn->close();
			return TRUE;
		} else {
		    echo "Error: " . $sql . "<br>" . $this->conn->error;die();
		}
    }
    
	public function read_perpindahan()
	{
		$con = $this->conn;
		$result = mysqli_query($con ,"
			select *, data_penduduk.nama_lengkap
			from data_pindah 
			join data_penduduk on data_penduduk.id_data_penduduk = data_pindah.data_penduduk_id_data_penduduk");
		return $result;
	}

	public function rekap_perpindahan()
	{
		$con = $this->conn;
		$result = mysqli_query($con ,"
			select count(*) as jumlah, MONTHNAME(tanggal_pindah) as bulan, MONTH(tanggal_pindah) as monthnumber
			from data_pindah 
			group by month(tanggal_pindah) order by month(tanggal_pindah) asc");
		return $result;
	}

	public function rekap_perpindahan_gender($jenis_kelamin, $month)
	{
		$con = $this->conn;
		$result = mysqli_query($con ,"
			select count(*) as jumlah, month(tanggal_pindah) as bulan
			from data_pindah 
			join data_penduduk on data_penduduk.id_data_penduduk = data_pindah.data_penduduk_id_data_penduduk
			where jenis_kelamin = '$jenis_kelamin'
			AND month(tanggal_pindah) = $month
			group by month(tanggal_pindah) order by month(tanggal_pindah) asc");
		return $result;
	}
}