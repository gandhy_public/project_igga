<?php

class model_kelahiran
{	

	protected $conn;

	public function __construct()
	{
		require_once (__DIR__ . '/../config/Database.php');
		$this->conn = (new koneksi())->connect();
	}

	public function add_kelahiran($nomor_kk,$nomor_nik,$nama_lengkap,$jenis_kelamin,$tempat_lahir,$hari_lahir,$tanggal_lahir,$jam_lahir,$tanggal_pengajuan_lahir,$nama_ibu,$alamat_ibu,$umur_ibu,$nama_ayah,$alamat_ayah,$umur_ayah)
	{
		$created_at = date('Y-m-d H:i:s');
		$sql = "INSERT INTO `data_kelahiran`(`nama`, `jenis_kelamin`, `tempat_lahir`, `hari_lahir`, `tanggal_lahir`, `jam_lahir`, `tanggal_pengajuan_lahir`, `nama_ayah`, `umur_ayah`, `alamat_ayah`, `nama_ibu`, `umur_ibu`, `alamat_ibu`, `nomor_nik`, `created_at`, `nomor_kk`) VALUES ('$nama_lengkap', '$jenis_kelamin', '$tempat_lahir', '$hari_lahir', '$tanggal_lahir', '$jam_lahir', '$tanggal_pengajuan_lahir', '$nama_ayah', '$umur_ayah', '$alamat_ayah', '$nama_ibu', '$umur_ibu', '$alamat_ibu', '$nomor_nik', '$created_at', '$nomor_kk')";

		if ($this->conn->query($sql) === TRUE) {
			$this->conn->close();
			return TRUE;
		} else {
		    echo "Error: " . $sql . "<br>" . $this->conn->error;die();
		}
		
	}

	public function read_kelahiran()
	{
		$stmt = $this->conn->prepare("SELECT * FROM `data_kelahiran` ORDER BY `id_data_kelahiran` DESC") or die($this->conn->error);
		if($stmt->execute()){
			$result = $stmt->get_result();
			return $result;
		}
	}

	public function hapus_kelahiran($id)
	{
		$con = $this->conn;
		$query = "delete from data_kelahiran where id_data_kelahiran = '$id'";
		if ($result = $con->query($query)) {
			return $result;
		} else {
			die(mysqli_error($con));
		}
	}

	public function hapus_penduduk($id)
	{
		$con = $this->conn;
		$query = "delete from data_penduduk where id_data_penduduk = '$id'";

		if ($result = $con->query($query)) {
			return $result;
		} else {
			die(mysqli_error($con));
		}
	}

	//Fungsi untuk edit menampilkan data yang dipilih 
	public function read_edit_kelahiran($id){
		$con = $this->conn;
		$result = mysqli_query($con ,"
			select *
			from data_kelahiran 
			where id_data_kelahiran = '$id'");
		return $result;
	}

	/*public function edit_penduduk($id_data_penduduk, $nik, $desa_id_desa, $nama_lengkap, $jenis_kelamin, $tempat_lahir, $tanggal_lahir, $agama, $pendidikan, $pekerjaan, $kewarganegaraan, $status_perkawinan, $alamat, $golongan_darah){
		$con = $this->conn;
		$result = mysqli_query($con,"update penduduk set nik='$nik', desa_id_desa='$desa_id_desa, nama_lengkap='$nama_lengkap', jenis_kelamin='$jenis_kelamin', tempat_lahir=$'tempat_lahir', tanggal_lahir=$'tanggal_lahir', agama='$agama', pendidikan='$pendidikan', pekerjaan='$pekerjaan', kewarganegaraan='$kewarganegaraan', status_perkawinan='$status_perkawinan', alamat='$alamat', golongan_darah='$golongan_darah'");
		return $result;
	}*/

	public function edit_kelahiran($nomor_kk,$nomor_nik,$nama_lengkap,$jenis_kelamin,$tempat_lahir,$hari_lahir,$tanggal_lahir,$jam_lahir,$tanggal_pengajuan_lahir,$nama_ibu,$alamat_ibu,$umur_ibu,$nama_ayah,$alamat_ayah,$umur_ayah,$id_data_kelahiran)
	{
		$sql = "
			UPDATE data_kelahiran
			SET 
				`nama`='$nama_lengkap',
				`jenis_kelamin`='$jenis_kelamin',
				`tempat_lahir`='$tempat_lahir',
				`hari_lahir`='$hari_lahir',
				`tanggal_lahir`='$tanggal_lahir',
				`jam_lahir`='$jam_lahir',
				`tanggal_pengajuan_lahir`='$tanggal_pengajuan_lahir',
				`nama_ayah`='$nama_ayah',
				`umur_ayah`='$umur_ayah',
				`alamat_ayah`='$alamat_ayah',
				`nama_ibu`='$nama_ibu',
				`umur_ibu`='$umur_ibu',
				`alamat_ibu`='$alamat_ibu',
				`nomor_nik`='$nomor_nik',
				`nomor_kk`='$nomor_kk'
			WHERE id_data_kelahiran = '$id_data_kelahiran'; 
		";

		if ($this->conn->query($sql) === TRUE) {
			$this->conn->close();

			return TRUE;
		} else {
		    echo "Error: " . $sql . "<br>" . $this->conn->error;die();
		}
	}

	public function rekap_kelahiran()
	{
		$con = $this->conn;
		$result = mysqli_query($con ,"
			select count(*) as jumlah, MONTHNAME(tanggal_lahir) as bulan, MONTH(tanggal_lahir) as monthnumber
			from data_kelahiran 
			group by month(tanggal_lahir) order by month(tanggal_lahir) asc");
		return $result;
	}

	public function rekap_kelahiran_gender($jenis_kelamin, $month)
	{
		$con = $this->conn;
		$result = mysqli_query($con ,"
			select count(*) as jumlah, month(tanggal_lahir) as bulan
			from data_kelahiran 
			where jenis_kelamin = '$jenis_kelamin'
			AND month(tanggal_lahir) = $month
			group by month(tanggal_lahir) order by month(tanggal_lahir) asc");
		return $result;
	}
}