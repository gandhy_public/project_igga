<?php

class model_penduduk
{	

	protected $conn;

	public function __construct()
	{
		require_once (__DIR__ . '/../config/Database.php');
		$this->conn = (new koneksi())->connect();
	}

	public function add_penduduk($nomor_kk, $nik, $nama_lengkap, $alamat, $tempat_lahir, $tanggal_lahir, $kewarganegaraan, $jenis_kelamin, $golongan_darah, $agama, $pendidikan, $pekerjaan, $status_perkawinan,$status_keluarga)
	{
		$created_at = date('Y-m-d H:i:s');
		$sql = "INSERT INTO `data_penduduk` (`nik`, `desa_id_desa`, `nama_lengkap`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `agama`, `pendidikan`, `pekerjaan`, `kewarganegaraan`, `status_perkawinan`, `alamat`, `golonga_darah`,`status_keluarga`, `created_at`) VALUES ('$nik', '1', '$nama_lengkap', '$jenis_kelamin', '$tempat_lahir', '$tanggal_lahir', '$agama', '$pendidikan', '$pekerjaan', '$kewarganegaraan', '$status_perkawinan', '$alamat', '$golongan_darah','$status_keluarga', '$created_at')";

		if ($this->conn->query($sql) === TRUE) {
		    $select_id = $this->conn->prepare("SELECT * FROM `data_penduduk` WHERE nik='$nik'") or die($this->conn->error);
			if($select_id->execute()){
				$result = $select_id->get_result();
				$result = $result->fetch_array();
				$this->add_to_kk($nik,$result[0],$nomor_kk);
			}

			$this->conn->close();
			return TRUE;
		} else {
		    echo "Error: " . $sql . "<br>" . $this->conn->error;die();
		}
		
	}

	public function add_to_kk($nik,$id,$nomor_kk)
	{
		$sql = "INSERT INTO `data_penduduk_kartu_keluarga` (`data_penduduk_nik`, `data_penduduk_id_data_penduduk`, `kartu_keluarga_nomer_kk`) VALUES ('$nik', '$id', '$nomor_kk')";
		if ($this->conn->query($sql) === TRUE) {
			$this->conn->close();
			return TRUE;
		} else {
		    echo "Error: " . $sql . "<br>" . $this->conn->error;die();
		}
	}
    
	public function read_penduduk()
	{
		$stmt = $this->conn->prepare("SELECT * FROM `data_penduduk` ORDER BY `id_data_penduduk` ASC") or die($this->conn->error);
		if($stmt->execute()){
			$result = $stmt->get_result();
			return $result;
		}
	}

	public function hapus_kk($id)
	{
		$con = $this->conn;
		$query = "delete from data_penduduk_kartu_keluarga where data_penduduk_id_data_penduduk = '$id'";

		if ($result = $con->query($query)) {
			return $result;
		} else {
			die(mysqli_error($con));
		}
	}

	public function hapus_penduduk($id)
	{
		$con = $this->conn;
		$query = "delete from data_penduduk where id_data_penduduk = '$id'";

		if ($result = $con->query($query)) {
			return $result;
		} else {
			die(mysqli_error($con));
		}
	}

	public function rekaplaki()
	{
		$stmt = $this->conn->prepare("SELECT COUNT(nik) as jumlah, data_penduduk.jenis_kelamin FROM `data_penduduk` WHERE jenis_kelamin='Laki-Laki' ") or die($this->conn->error);
		if($stmt->execute()){
			$result = $stmt->get_result();
			return $result;
		}
	}
	public function rekapwanita()
	{
		$stmt = $this->conn->prepare("SELECT COUNT(nik) as jumlah, data_penduduk.jenis_kelamin FROM `data_penduduk` WHERE jenis_kelamin='Perempuan'") or die($this->conn->error);
		if($stmt->execute()){
			$result = $stmt->get_result();
			return $result;
		}
	}
	//Fungsi untuk edit menampilkan data yang dipilih 
	public function read_edit_penduduk($nik){
		$con = $this->conn;
		$result = mysqli_query($con ,"
			select *, a.kartu_keluarga_nomer_kk as nomor_kk
			from data_penduduk 
			join data_penduduk_kartu_keluarga as a on a.data_penduduk_nik = data_penduduk.nik
			where data_penduduk.nik = '$nik'");
		return $result;
	}

	/*public function edit_penduduk($id_data_penduduk, $nik, $desa_id_desa, $nama_lengkap, $jenis_kelamin, $tempat_lahir, $tanggal_lahir, $agama, $pendidikan, $pekerjaan, $kewarganegaraan, $status_perkawinan, $alamat, $golongan_darah){
		$con = $this->conn;
		$result = mysqli_query($con,"update penduduk set nik='$nik', desa_id_desa='$desa_id_desa, nama_lengkap='$nama_lengkap', jenis_kelamin='$jenis_kelamin', tempat_lahir=$'tempat_lahir', tanggal_lahir=$'tanggal_lahir', agama='$agama', pendidikan='$pendidikan', pekerjaan='$pekerjaan', kewarganegaraan='$kewarganegaraan', status_perkawinan='$status_perkawinan', alamat='$alamat', golongan_darah='$golongan_darah'");
		return $result;
	}*/

	public function edit_penduduk($id_data_penduduk, $nomor_kk, $nik, $nama_lengkap, $alamat, $tempat_lahir, $tanggal_lahir, $kewarganegaraan, $jenis_kelamin, $golongan_darah, $agama, $pendidikan, $pekerjaan, $status_perkawinan)
	{
		$sql = "
			UPDATE data_penduduk
			SET 
				nik = '$nik',
				nama_lengkap = '$nama_lengkap',
				jenis_kelamin = '$jenis_kelamin',
				tempat_lahir = '$tempat_lahir',
				tanggal_lahir = '$tanggal_lahir',
				agama = '$agama',
				pendidikan = '$pendidikan',
				pekerjaan = '$pekerjaan',
				kewarganegaraan = '$kewarganegaraan',
				status_perkawinan = '$status_perkawinan',
				alamat = '$alamat',
				golonga_darah = '$golongan_darah'
			WHERE id_data_penduduk = '$id_data_penduduk'; 
		";

		$this->hapus_kk($id_data_penduduk);
		if ($this->conn->query($sql) === TRUE) {
			$this->add_to_kk($nik,$id_data_penduduk,$nomor_kk);
			$this->conn->close();

			return TRUE;
		} else {
		    echo "Error: " . $sql . "<br>" . $this->conn->error;die();
		}
	}

	public function rekap_penduduk()
	{
		$con = $this->conn;
		$result = mysqli_query($con ,"
			select count(*) as jumlah, MONTHNAME(created_at) as bulan, MONTH(created_at) as monthnumber
			from data_penduduk 
			group by month(created_at) order by month(created_at) asc");
		return $result;
	}

	public function rekap_penduduk_gender($jenis_kelamin, $month)
	{
		$con = $this->conn;
		$result = mysqli_query($con ,"
			select count(*) as jumlah, month(created_at) as bulan
			from data_penduduk 
			where jenis_kelamin = '$jenis_kelamin'
			AND month(created_at) = $month
			group by month(created_at) order by month(created_at) asc");
		return $result;
	}
}