<?php

class model_kematian
{

    protected $conn;

    public function __construct()
    {
        require_once (__DIR__ . '/../config/Database.php');
        $this->conn = (new koneksi())->connect();
    }

    public function add_kematian($nik, $penduduk_id, $tempat_meninggal, $hari_meninggal, $tanggal_meninggal, $usia_meninggal, $penyebab_kematian, $tanggal_pengajuan_meninggal)
    {
        $sql = "INSERT INTO `data_kematian` (`data_penduduk_nik`, `data_penduduk_id_data_penduduk`, `tempat_meninggal`, `hari_meninggal`, `tanggal_meninggal`, `usia_meninggal`, `penyebab_kematian`, `tanggal_pengajuan_meniggal`) VALUES ('$nik', '$penduduk_id', '$tempat_meninggal', '$hari_meninggal', '$tanggal_meninggal', '$usia_meninggal', '$penyebab_kematian', '$tanggal_pengajuan_meninggal')";

        if ($this->conn->query($sql) === TRUE) {
            $delete = $this->conn->prepare("DELETE FROM `data_penduduk_kartu_keluarga` WHERE data_penduduk_id_data_penduduk='$penduduk_id'") or die($this->conn->error);
            $delete->execute();

            $this->conn->close();
            return TRUE;
        } else {
            echo "Error: " . $sql . "<br>" . $this->conn->error;die();
        }
    }

    public function read_kematian()
    {
        $stmt = $this->conn->prepare("SELECT `data_kematian`.*, `data_penduduk`.* FROM `data_kematian` LEFT JOIN `data_penduduk` ON `data_kematian`.`data_penduduk_id_data_penduduk` = `data_penduduk`.`id_data_penduduk` ORDER BY `data_kematian`.`id_data_kematian` ASC") or die($this->conn->error);
        if($stmt->execute()){
            $result = $stmt->get_result();
            return $result;
        }
    }


    public function hapus($id_data_kematian)
    {
        $con = $this->conn;
        mysqli_query($con ,"delete from data_kematian where id_data_kematian = '$id_data_kematian'");
    }
}