<?php

class model_kartu_keluarga
{	

	protected $conn;

	public function __construct()
	{
		require_once (__DIR__ . '/../config/Database.php');
		$this->conn = (new koneksi())->connect();
	}

	public function add_kk($nomor_kk, $nama_kk)
	{

		$stmt = $this->conn->prepare ("INSERT INTO `kartu_keluarga` (`nomer_kk`, `kepala_keluarga`)
        VALUES (?, ?)") or die($this->conn->error);
        //mengikuti character sesuai database
		$stmt->bind_param("is", $nomor_kk, $nama_kk);
		if($stmt->execute())
		{
            $stmt->close();
			$this->conn->close();
			return true;
		}
    }

    public function read_kk()
	{
		$stmt = $this->conn->prepare("SELECT * FROM kartu_keluarga") or die($this->conn->error);
		if($stmt->execute()){
			$result = $stmt->get_result();
			return $result;
		}
	}

	public function detail_kk($nomerkk)
	{
		$stmt = $this->conn->prepare("SELECT * FROM `data_penduduk_kartu_keluarga` JOIN kartu_keluarga ON kartu_keluarga.nomer_kk=data_penduduk_kartu_keluarga.kartu_keluarga_nomer_kk JOIN data_penduduk ON data_penduduk.nik=data_penduduk_kartu_keluarga.data_penduduk_nik where kartu_keluarga.nomer_kk='$nomerkk'") or die($this->conn->error);
		if($stmt->execute()){
			$result = $stmt->get_result();
			return $result;
		}
	}

	public function hapus($nomor_kk)
	{
		$con = $this->conn;
		mysqli_query($con ,"delete from kartu_keluarga where nomer_kk = '$nomor_kk'");
	}

	//Fungsi untuk edit menampilkan data yang dipilih 
	public function read_edit_kk($nomor_kk){
		$con = $this->conn;
		$result = mysqli_query($con ,"select * from kartu_keluarga where nomer_kk = '$nomor_kk'");
		return $result;
	}

	public function edit_kk($nomor_kk, $nama_kk){
		$con = $this->conn;
		$query = "UPDATE kartu_keluarga
			SET kepala_keluarga = '$nama_kk'
			WHERE nomer_kk = $nomor_kk";

		$result = mysqli_query($con,$query);
		return $result;
	}
}