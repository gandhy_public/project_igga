<?php
//ke model
	require_once '../models/KelahiranModel.php';
	$conn = new model_kelahiran();
//TAMBAH
//action = ke view
	$action = $_GET['action'];
	if($action == "tambah_kelahiran")
	{ 	
		$nomor_kk = $_POST['nomor_kk'];
		$nomor_nik = $_POST['nomor_nik'];
		$nama_lengkap = $_POST['nama_lengkap'];
		$jenis_kelamin = $_POST['jenis_kelamin'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$hari_lahir = $_POST['hari_lahir'];
		$tanggal_lahir = $_POST['tanggal_lahir'];
		$jam_lahir = $_POST['jam_lahir'];
		$tanggal_pengajuan_lahir = $_POST['tanggal_pengajuan_lahir'];
		$nama_ibu = $_POST['nama_ibu'];
		$alamat_ibu = $_POST['alamat_ibu'];
		$umur_ibu = $_POST['umur_ibu'];
		$nama_ayah = $_POST['nama_ayah'];
		$alamat_ayah = $_POST['alamat_ayah'];
		$umur_ayah = $_POST['umur_ayah'];

		$conn->add_kelahiran($nomor_kk,$nomor_nik,$nama_lengkap,$jenis_kelamin,$tempat_lahir,$hari_lahir,$tanggal_lahir,$jam_lahir,$tanggal_pengajuan_lahir,$nama_ibu,$alamat_ibu,$umur_ibu,$nama_ayah,$alamat_ayah,$umur_ayah);

		//page view yang akan dituju
		header("location:../public/kelahiran/index.php");		
//HAPUS
//action = ke view	
	}
	elseif($action == "hapus_kelahiran")
	{ 	
		$conn->hapus_kelahiran($_GET['id']);
		header("location:../public/kelahiran/index.php");
	}
//EDIT
//action = ke view
	//}
	elseif($action == "edit_kelahiran")
	{
		//print("<pre>".print_r($_POST,true)."</pre>");die();

		$nomor_kk = $_POST['nomor_kk'];
		$nomor_nik = $_POST['nomor_nik'];
		$nama_lengkap = $_POST['nama_lengkap'];
		$jenis_kelamin = $_POST['jenis_kelamin'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$hari_lahir = $_POST['hari_lahir'];
		$tanggal_lahir = $_POST['tanggal_lahir'];
		$jam_lahir = $_POST['jam_lahir'];
		$tanggal_pengajuan_lahir = $_POST['tanggal_pengajuan_lahir'];
		$nama_ibu = $_POST['nama_ibu'];
		$alamat_ibu = $_POST['alamat_ibu'];
		$umur_ibu = $_POST['umur_ibu'];
		$nama_ayah = $_POST['nama_ayah'];
		$alamat_ayah = $_POST['alamat_ayah'];
		$umur_ayah = $_POST['umur_ayah'];
		$id_data_kelahiran = $_POST['id_data_kelahiran'];

		$conn->edit_kelahiran($nomor_kk,$nomor_nik,$nama_lengkap,$jenis_kelamin,$tempat_lahir,$hari_lahir,$tanggal_lahir,$jam_lahir,$tanggal_pengajuan_lahir,$nama_ibu,$alamat_ibu,$umur_ibu,$nama_ayah,$alamat_ayah,$umur_ayah,$id_data_kelahiran);

		//page view yang akan dituju
		header("location:../public/kelahiran/index.php");		
	}

	
?>