<?php

class UserController
{

	protected $userModel;

	public function __construct()
	{
		require_once(__DIR__ . "/../models/UserModel.php");
		$this->userModel = new UserModel();
	}

	/**
	 * Get all user data and show it
	 */
	public function index()
	{
		$data = $this->userModel->getAll();
		return $data;
	}
}