<?php
//ke model
	require_once '../models/PerpindahanModel.php';
	$conn = new model_perpindahan();
//TAMBAH
//action = ke view
	$action = $_GET['action'];
	if($action == "tambah_perpindahan")
	{ 	
		$penduduk 							= explode(' - ', $_POST['nik']);
		$data_penduduk_nik 					= $penduduk[1];
		$data_penduduk_id_data_penduduk 	= $penduduk[0];
		$alasan_pindah 						= $_POST['alasan_pindah'];
		$alamat_pindah 						= $_POST['alamat_pindah'];
		$tanggal_pindah 					= $_POST['tanggal_pindah'];

			//conn = ke model
			$conn->add_perpindahan($data_penduduk_nik, $data_penduduk_id_data_penduduk, $alasan_pindah, $alamat_pindah, $tanggal_pindah);
			//page view yang akan dituju
		   	header("location:../public/perpindahan/index.php");		
//HAPUS
//action = ke view	
	}
	elseif($action == "hapus_penduduk")
	{ 	
		$conn->hapus_kk($_GET['id']);
		$conn->hapus_penduduk($_GET['id']);
		header("location:../public/penduduk/index.php");
	}
//EDIT
//action = ke view
	//}
	elseif($action == "edit_penduduk")
	{
		//print("<pre>".print_r($_POST,true)."</pre>");die();

		$nomor_kk = $_POST['nomor_kk'];
		$nik = $_POST['nik'];
		$nama_lengkap = $_POST['nama_lengkap'];
		$alamat = $_POST['alamat'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$tanggal_lahir = $_POST['tanggal_lahir'];
		$kewarganegaraan = $_POST['kewarganegaraan'];
		$jenis_kelamin = $_POST['jenis_kelamin'];
		$golongan_darah = $_POST['golongan_darah'];
		$agama = $_POST['agama'];
		$pendidikan = $_POST['pendidikan'];
		$pekerjaan = $_POST['pekerjaan'];
		$status_perkawinan = $_POST['status_perkawinan'];
		$id_data_penduduk = $_POST['id_data_penduduk'];

		$conn->edit_penduduk($id_data_penduduk, $nomor_kk, $nik, $nama_lengkap, $alamat, $tempat_lahir, $tanggal_lahir, $kewarganegaraan, $jenis_kelamin, $golongan_darah, $agama, $pendidikan, $pekerjaan, $status_perkawinan);

		header("location:../public/penduduk/index.php");
	}

	
?>