<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//ke model
    require_once '../models/KematianModel.php';
    $conn = new model_kematian();
//TAMBAH
//action = ke view
    $action = $_GET['action'];
    if($action == "tambah_kematian")
    {
            $penduduk_nik = explode(';', $_POST['penduduk_nik']);

            $nik = $penduduk_nik[0];
            $penduduk_id = $penduduk_nik[1];
            $tempat_meninggal = $_POST['tempat_meninggal'];
            $hari_meninggal = $_POST['hari_meninggal'];
            $tanggal_meninggal = $_POST['tanggal_meninggal'];
            $usia_meninggal = $_POST['usia_meninggal'];
            $penyebab_kematian = $_POST['penyebab_kematian'];
            $tanggal_pengajuan_meninggal = $_POST['tanggal_pengajuan_meninggal'];
            //conn = ke model
            $conn->add_kematian($nik, $penduduk_id, $tempat_meninggal, $hari_meninggal, $tanggal_meninggal, $usia_meninggal, $penyebab_kematian, $tanggal_pengajuan_meninggal);
            //page view yang akan dituju
            header("location:../public/kematian/index.php");
//HAPUS
//action = ke view
    }
    elseif($action == "hapus_kematian")
    {
        $conn->hapus($_GET['id']);
        header("location:../public/kematian/index.php");
    }


?>