<?php
//ke model
	require_once '../models/PendudukModel.php';
	$conn = new model_penduduk();
//TAMBAH
//action = ke view
	$action = $_GET['action'];
	if($action == "tambah_penduduk")
	{ 	
		$nomor_kk = $_POST['nomor_kk'];
		$nik = $_POST['nik'];
		$nama_lengkap = $_POST['nama_lengkap'];
		$alamat = $_POST['alamat'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$tanggal_lahir = $_POST['tanggal_lahir'];
		$kewarganegaraan = $_POST['kewarganegaraan'];
		$jenis_kelamin = $_POST['jenis_kelamin'];
		$golongan_darah = $_POST['golongan_darah'];
		$agama = $_POST['agama'];
		$pendidikan = $_POST['pendidikan'];
		$pekerjaan = $_POST['pekerjaan'];
		$status_perkawinan = $_POST['status_perkawinan'];
		$status_keluarga = $_POST['status_keluarga'];

			//conn = ke model
			$conn->add_penduduk($nomor_kk, $nik, $nama_lengkap, $alamat, $tempat_lahir, $tanggal_lahir, $kewarganegaraan, $jenis_kelamin, $golongan_darah, $agama, $pendidikan, $pekerjaan, $status_perkawinan,$status_keluarga);
			//page view yang akan dituju
		   	header("location:../public/penduduk/index.php");		
//HAPUS
//action = ke view	
	}
	elseif($action == "hapus_penduduk")
	{ 	
		$conn->hapus_kk($_GET['id']);
		$conn->hapus_penduduk($_GET['id']);
		header("location:../public/penduduk/index.php");
	}
//EDIT
//action = ke view
	//}
	elseif($action == "edit_penduduk")
	{
		//print("<pre>".print_r($_POST,true)."</pre>");die();

		$nomor_kk = $_POST['nomor_kk'];
		$nik = $_POST['nik'];
		$nama_lengkap = $_POST['nama_lengkap'];
		$alamat = $_POST['alamat'];
		$tempat_lahir = $_POST['tempat_lahir'];
		$tanggal_lahir = $_POST['tanggal_lahir'];
		$kewarganegaraan = $_POST['kewarganegaraan'];
		$jenis_kelamin = $_POST['jenis_kelamin'];
		$golongan_darah = $_POST['golongan_darah'];
		$agama = $_POST['agama'];
		$pendidikan = $_POST['pendidikan'];
		$pekerjaan = $_POST['pekerjaan'];
		$status_perkawinan = $_POST['status_perkawinan'];
		$id_data_penduduk = $_POST['id_data_penduduk'];

		$conn->edit_penduduk($id_data_penduduk, $nomor_kk, $nik, $nama_lengkap, $alamat, $tempat_lahir, $tanggal_lahir, $kewarganegaraan, $jenis_kelamin, $golongan_darah, $agama, $pendidikan, $pekerjaan, $status_perkawinan);

		header("location:../public/penduduk/index.php");
	}

	
?>