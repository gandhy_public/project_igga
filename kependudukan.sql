-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2019 at 05:29 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `free_kependudukan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(30) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `password_2` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_anggota_keluarga`
--

CREATE TABLE `data_anggota_keluarga` (
  `id` int(11) NOT NULL,
  `id_data_penduduk` int(11) NOT NULL,
  `nomer_kk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_kelahiran`
--

CREATE TABLE `data_kelahiran` (
  `id_data_kelahiran` int(20) UNSIGNED NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jenis_kelamin` enum('Laki-laki','Perempuan') DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `hari_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jam_lahir` time DEFAULT NULL,
  `tanggal_pengajuan_lahir` date DEFAULT NULL,
  `nama_ayah` varchar(255) DEFAULT NULL,
  `umur_ayah` int(16) UNSIGNED DEFAULT NULL,
  `alamat_ayah` varchar(255) DEFAULT NULL,
  `nama_ibu` varchar(255) DEFAULT NULL,
  `umur_ibu` int(26) UNSIGNED DEFAULT NULL,
  `alamat_ibu` varchar(255) DEFAULT NULL,
  `nomor_nik` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nomor_kk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_kelahiran`
--

INSERT INTO `data_kelahiran` (`id_data_kelahiran`, `nama`, `jenis_kelamin`, `tempat_lahir`, `hari_lahir`, `tanggal_lahir`, `jam_lahir`, `tanggal_pengajuan_lahir`, `nama_ayah`, `umur_ayah`, `alamat_ayah`, `nama_ibu`, `umur_ibu`, `alamat_ibu`, `nomor_nik`, `created_at`, `nomor_kk`) VALUES
(3, 'Bayi Gandhy Bagoes', 'Perempuan', 'Malanggg', 'Kamisss', '2019-12-31', '12:59:00', '2019-12-31', 'Ayah Bayi', 2, 'Malang', 'Ibu Bayi', 25, 'Malang', '12345555', '2019-07-18 15:26:47', '11111');

-- --------------------------------------------------------

--
-- Table structure for table `data_kematian`
--

CREATE TABLE `data_kematian` (
  `id_data_kematian` int(30) UNSIGNED NOT NULL,
  `data_penduduk_nik` varchar(16) NOT NULL,
  `data_penduduk_id_data_penduduk` int(255) UNSIGNED NOT NULL,
  `tempat_meninggal` varchar(255) DEFAULT NULL,
  `hari_meninggal` varchar(10) DEFAULT NULL,
  `tanggal_meninggal` date DEFAULT NULL,
  `usia_meninggal` varchar(10) DEFAULT NULL,
  `penyebab_kematian` varchar(255) DEFAULT NULL,
  `tanggal_pengajuan_meniggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_penduduk`
--

CREATE TABLE `data_penduduk` (
  `id_data_penduduk` int(255) UNSIGNED NOT NULL,
  `nik` varchar(16) NOT NULL,
  `desa_id_desa` int(16) UNSIGNED NOT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `jenis_kelamin` enum('Laki-Laki','Perempuan') DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `pendidikan` varchar(200) DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `kewarganegaraan` varchar(255) DEFAULT NULL,
  `status_perkawinan` enum('Kawin','Belum Kawin') DEFAULT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `golonga_darah` enum('A','B','AB','O') DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_penduduk`
--

INSERT INTO `data_penduduk` (`id_data_penduduk`, `nik`, `desa_id_desa`, `nama_lengkap`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `agama`, `pendidikan`, `pekerjaan`, `kewarganegaraan`, `status_perkawinan`, `alamat`, `golonga_darah`, `created_at`) VALUES
(1, '123456789', 1, 'ahmad zainal', 'Laki-Laki', 'malang', '2019-07-18', 'islam', 's1', 'swasta', 'indonesia', 'Kawin', 'desa besole kecamatan besuki', 'A', '2019-07-17 13:41:05'),
(2, '987654321', 1, 'nur hayati', 'Perempuan', 'tulungagung', '2019-07-01', 'islam', 'sma', 'swasta', 'indonesia', 'Kawin', 'bandung', 'B', '2019-07-17 13:41:05'),
(4, '124567489', 2, 'ananda aprilia', 'Perempuan', 'jombang', '2019-07-02', 'islam', 's1', 'pns', 'indonesia', 'Kawin', 'tanggulwelahan', 'A', '2019-07-17 13:41:05'),
(8, '108354781', 1, 'nur aini', 'Perempuan', 'lamongan', '2019-07-02', 'islam', 'd3', 'pengusaha', 'indonesia', 'Belum Kawin', 'rejotangan', 'AB', '2019-07-17 13:41:05'),
(12, '22334455', 2, 'maulana dendi', 'Laki-Laki', 'malang', '2019-07-04', 'islam', 'd3', 'swasta', 'indonesia', 'Belum Kawin', 'malang', 'B', '2019-07-17 13:41:05');

-- --------------------------------------------------------

--
-- Table structure for table `data_penduduk_kartu_keluarga`
--

CREATE TABLE `data_penduduk_kartu_keluarga` (
  `data_penduduk_nik` varchar(16) NOT NULL,
  `data_penduduk_id_data_penduduk` int(255) UNSIGNED NOT NULL,
  `kartu_keluarga_nomer_kk` int(255) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_penduduk_kartu_keluarga`
--

INSERT INTO `data_penduduk_kartu_keluarga` (`data_penduduk_nik`, `data_penduduk_id_data_penduduk`, `kartu_keluarga_nomer_kk`) VALUES
('108354781', 8, 99887766),
('124567489', 4, 99887766),
('22334455', 12, 11223344);

-- --------------------------------------------------------

--
-- Table structure for table `data_pindah`
--

CREATE TABLE `data_pindah` (
  `id_data_pindah` int(10) UNSIGNED NOT NULL,
  `data_penduduk_nik` varchar(16) NOT NULL,
  `data_penduduk_id_data_penduduk` int(255) UNSIGNED NOT NULL,
  `alasan_pindah` varchar(255) DEFAULT NULL,
  `alamat_pindah` varchar(255) DEFAULT NULL,
  `tanggal_pindah` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `desa`
--

CREATE TABLE `desa` (
  `id_desa` int(16) UNSIGNED NOT NULL,
  `nama_desa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desa`
--

INSERT INTO `desa` (`id_desa`, `nama_desa`) VALUES
(1, 'besole'),
(2, 'besuki'),
(3, 'Coba Desa Testing'),
(4, 'Testing Add');

-- --------------------------------------------------------

--
-- Table structure for table `kartu_keluarga`
--

CREATE TABLE `kartu_keluarga` (
  `nomer_kk` int(255) UNSIGNED NOT NULL,
  `kepala_keluarga` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kartu_keluarga`
--

INSERT INTO `kartu_keluarga` (`nomer_kk`, `kepala_keluarga`) VALUES
(11111, 'Gandhy Bagoes'),
(11223344, 'maulana dendi'),
(99887766, 'ahmad zainal');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `data_anggota_keluarga`
--
ALTER TABLE `data_anggota_keluarga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_kelahiran`
--
ALTER TABLE `data_kelahiran`
  ADD PRIMARY KEY (`id_data_kelahiran`);

--
-- Indexes for table `data_kematian`
--
ALTER TABLE `data_kematian`
  ADD PRIMARY KEY (`id_data_kematian`),
  ADD KEY `data_kematian_FKIndex1` (`data_penduduk_id_data_penduduk`,`data_penduduk_nik`);

--
-- Indexes for table `data_penduduk`
--
ALTER TABLE `data_penduduk`
  ADD PRIMARY KEY (`id_data_penduduk`,`nik`),
  ADD KEY `data_penduduk_FKIndex1` (`desa_id_desa`);

--
-- Indexes for table `data_penduduk_kartu_keluarga`
--
ALTER TABLE `data_penduduk_kartu_keluarga`
  ADD PRIMARY KEY (`data_penduduk_nik`,`data_penduduk_id_data_penduduk`,`kartu_keluarga_nomer_kk`),
  ADD KEY `data_penduduk_has_kartu_keluarga_FKIndex1` (`data_penduduk_id_data_penduduk`,`data_penduduk_nik`),
  ADD KEY `data_penduduk_has_kartu_keluarga_FKIndex2` (`kartu_keluarga_nomer_kk`);

--
-- Indexes for table `data_pindah`
--
ALTER TABLE `data_pindah`
  ADD PRIMARY KEY (`id_data_pindah`),
  ADD KEY `data_pindah_FKIndex1` (`data_penduduk_id_data_penduduk`,`data_penduduk_nik`);

--
-- Indexes for table `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`id_desa`);

--
-- Indexes for table `kartu_keluarga`
--
ALTER TABLE `kartu_keluarga`
  ADD PRIMARY KEY (`nomer_kk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(30) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_anggota_keluarga`
--
ALTER TABLE `data_anggota_keluarga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_kelahiran`
--
ALTER TABLE `data_kelahiran`
  MODIFY `id_data_kelahiran` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_kematian`
--
ALTER TABLE `data_kematian`
  MODIFY `id_data_kematian` int(30) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_penduduk`
--
ALTER TABLE `data_penduduk`
  MODIFY `id_data_penduduk` int(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `data_pindah`
--
ALTER TABLE `data_pindah`
  MODIFY `id_data_pindah` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `desa`
--
ALTER TABLE `desa`
  MODIFY `id_desa` int(16) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_kematian`
--
ALTER TABLE `data_kematian`
  ADD CONSTRAINT `data_kematian_ibfk_1` FOREIGN KEY (`data_penduduk_id_data_penduduk`,`data_penduduk_nik`) REFERENCES `data_penduduk` (`id_data_penduduk`, `nik`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `data_penduduk`
--
ALTER TABLE `data_penduduk`
  ADD CONSTRAINT `data_penduduk_ibfk_1` FOREIGN KEY (`desa_id_desa`) REFERENCES `desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `data_penduduk_kartu_keluarga`
--
ALTER TABLE `data_penduduk_kartu_keluarga`
  ADD CONSTRAINT `data_penduduk_kartu_keluarga_ibfk_1` FOREIGN KEY (`data_penduduk_id_data_penduduk`,`data_penduduk_nik`) REFERENCES `data_penduduk` (`id_data_penduduk`, `nik`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `data_penduduk_kartu_keluarga_ibfk_2` FOREIGN KEY (`kartu_keluarga_nomer_kk`) REFERENCES `kartu_keluarga` (`nomer_kk`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `data_pindah`
--
ALTER TABLE `data_pindah`
  ADD CONSTRAINT `data_pindah_ibfk_1` FOREIGN KEY (`data_penduduk_id_data_penduduk`,`data_penduduk_nik`) REFERENCES `data_penduduk` (`id_data_penduduk`, `nik`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
