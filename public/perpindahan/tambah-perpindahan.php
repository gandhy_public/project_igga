<!DOCTYPE html>
<html lang="en">
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php");?>

<?php
require '../../models/PendudukModel.php';
$conn = new model_penduduk();
?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Perpindahan <small>| Tambah Perpindahan</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Perpindahan</a></li>
					<li class="active">Tambah Perpindahan</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-primary">
			    			<div class="panel-heading">
			    				<h3 class="panel-title">Tambah Perpindahan Baru</h3>
			    			</div>
			    			<form action="../../controllers/PerpindahanController.php?action=tambah_perpindahan" class="form-horizontal" method="POST">
			    				<div class="panel-body">
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nomor NIK</label>
			    						<div class="col-md-10">
			    							<select name="nik" id="nik" class="form-control">
			    							<?php
					                   			$tampil = $conn->read_penduduk();
								                while($fetch = $tampil->fetch_array())
								                { 
							                ?>
			    								<option value="<?= $fetch['id_data_penduduk'].' - '.$fetch['nik']?>"><?= $fetch['nik']?> - <?= $fetch['nama_lengkap']?></option>
			    							<?php }?>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Alamat Pindah</label>
			    						<div class="col-md-10">
			    							<textarea name="alamat_pindah" id="alamat_pindah" cols="30" rows="3" class="form-control"></textarea>
			    						</div>
			    					</div>

			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Alasan Pindah</label>
			    						<div class="col-md-10">
			    							<textarea name="alasan_pindah" id="alasan_pindah" cols="30" rows="5" class="form-control"></textarea>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tanggal Pindah</label>
			    						<div class="col-md-10">
			    							<input type="date" name="tanggal_pindah" id="tanggal_pindah" class="form-control">
			    						</div>
			    					</div>
			    				<div class="panel-footer" align="right">
			    					<a href="index.php"><button class="btn btn-danger"><i class="fa fa-times"></i> Batal</button></a>
			    					<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
			    				</div>
			    			</form>
			    		</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>		
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>