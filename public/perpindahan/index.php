<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/PerpindahanModel.php';
$conn = new model_perpindahan();
?>
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Perpindahan
					<small>| Daftar Perpindahan</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Perpindahan</a></li>
					<li class="active">Daftar Perpindahan</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
				    		<div class="panel-body">
				    			<table class="table table-striped table-hover">
				    				<thead>
				    					<tr>
				    						<th>NIK</th>
				    						<th>Nama</th>
				    						<th>Tanggal Pindah</th>
				    						<th>Alasan Pindah</th>
				    					</tr>
				    				</thead>
				    				<tbody>
				    					<?php
				    					$nomor = 1;
				    					$read_penduduk = $conn->read_perpindahan();
				    					while($fetch = $read_penduduk->fetch_array()){
				    						?>
				    					<tr>
			    							<td><?php echo $fetch['data_penduduk_nik']?></td>
			    							<td><?php echo $fetch['nama_lengkap']?></td>
			    							<td><?php echo $fetch['tanggal_pindah']?></td>
			    							<td><?php echo $fetch['alasan_pindah']?></td>
				    					</tr>
				    				<?php
				    				}
				    				?>
				    				</tbody>
				    			</table>
				    		</div>
				    	</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>

	</div>

<!-- Load javascripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>