<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/PerpindahanModel.php';
$conn = new model_perpindahan();
?>
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Rekap
					<small>| Perpindahan</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Rekap</a></li>
					<li class="active">Perpindahan</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
				    		<div class="panel-body">
				    			<table class="table table-striped table-hover">
				    				<thead>
				    					<tr>
				    						<th>Nomor</th>
				    						<th>Bulan</th>
				    						<th>Jumlah Perpindahan</th>
				    						<th>Laki-Laki</th>
				    						<th>Perempuan</th>
				    					</tr>
				    				</thead>
				    				<tbody>
				    					<?php
				    					$nomor = 1;
				    					$read_kelahiran = $conn->rekap_perpindahan();
				    					while($fetch = $read_kelahiran->fetch_array()){
				    						?>
				    					<tr>
				    						<td><?php echo $nomor++ ?>.</td>
			    							<td><?php echo $fetch['bulan']?></td>
			    							<td><?php echo $fetch['jumlah']?></td>
			    							<td>
			    								<?php 
			    								$laki = $conn->rekap_perpindahan_gender('Laki-laki', $fetch['monthnumber']);
			    								$laki = $laki->fetch_array();
			    								echo (count($laki[0]) == 0) ? 0 : $laki[0]['jumlah'] 
			    								?>
			    							</td>
			    							<td>
			    								<?php 
			    								$laki = $conn->rekap_perpindahan_gender('Perempuan', $fetch['monthnumber']);
			    								$laki = $laki->fetch_array();
			    								echo (count($laki[0]) == 0) ? 0 : $laki[0]['jumlah'] 
			    								?>
			    							</td>
				    					</tr>
				    					<?php }?>
				    				</tbody>
				    			</table>
				    		</div>
				    	</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>