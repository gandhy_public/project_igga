<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/PendudukModel.php';
$conn = new model_penduduk();
?>
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Penduduk
					<small>| Daftar Penduduk</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Penduduk</a></li>
					<li class="active">Daftar Penduduk</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
				    		<div class="panel-body">
				    			<table class="table table-striped table-hover">
				    				<thead>
				    					<tr>
				    						<th>nomor</th>
				    						<th>NIK</th>
				    						<th>Nama</th>
				    						<th>Alamat</th>
				    						<th>Action</th>
				    					</tr>
				    				</thead>
				    				<tbody>
				    					<?php
				    					$nomor = 1;
				    					$read_penduduk = $conn->read_penduduk();
				    					while($fetch = $read_penduduk->fetch_array()){
				    						?>
				    					<tr>
				    							<td><?php echo $nomor++ ?>.</td>
				    							<td><?php echo $fetch['nik']?></td>
				    							<td><?php echo $fetch['nama_lengkap']?></td>
				    							<td><?php echo $fetch['alamat']?></td>

				    							<td>
				    							<!--HAPUS DATA-->
				    							<a class="btn btn-danger btn-sm" href="../../controllers/PendudukController.php?id=<?php echo $fetch['id_data_penduduk']?>&action=hapus_penduduk"><i class="fa fa-trash"></i></a>
				    							<!--EDIT DATA-->
				    							<a class="btn btn-warning btn-sm" href="edit.php?nik=<?php echo $fetch['nik'];?>"><i class="fa fa-pencil-square-o"></i></a>
				    					</tr>
				    				<?php
				    				}
				    				?>
				    				</tbody>
				    			</table>
				    		</div>
				    	</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>

	</div>

<!-- Load javascripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>