<!DOCTYPE html>
<html lang="en">
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php");?>

<?php
require '../../models/KartuKeluargaModel.php';
$conn = new model_kartu_keluarga();
?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Penduduk <small>| Tambah Penduduk</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Penduduk</a></li>
					<li class="active">Tambah Penduduk</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-primary">
			    			<div class="panel-heading">
			    				<h3 class="panel-title">Tambah Penduduk Baru</h3>
			    			</div>
			    			<form action="../../controllers/PendudukController.php?action=tambah_penduduk" class="form-horizontal" method="POST">
			    				<div class="panel-body">
			    					<!-- <div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nomor KK</label>
			    						<div class="col-md-10">
			    							<select name="nomor_kk" id="nomor_kk" class="form-control">
			    							<?php
					                   			$tampil = $conn->read_kk();
								                while($fetch = $tampil->fetch_array())
								                { 
							                ?>
			    								<option value="<?= $fetch['nomer_kk']?>"><?= $fetch['nomer_kk']?> - <?= $fetch['kepala_keluarga']?></option>
			    							<?php }?>
			    							</select>
			    						</div>
			    					</div> -->
			    					<div class="form-group">
			    						
			    						<div class="col-md-10">
			    							<input type="hidden" name="nomor_kk" value="<?php echo $_GET['nomor_kk']; ?>" id="nik" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nomor NIK</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nik" id="nik" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nama</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Alamat</label>
			    						<div class="col-md-10">
			    							<textarea name="alamat" id="alamat" cols="30" rows="5" class="form-control"></textarea>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tempat Lahir</label>
			    						<div class="col-md-10">
			    							<input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tanggal Lahir</label>
			    						<div class="col-md-10">
			    							<input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Kewarganegaraan</label>
			    						<div class="col-md-10">
			    							<input type="text" name="kewarganegaraan" id="kewarganegaraan" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Jenis Kelamin</label>
			    						<div class="col-md-10">
			    							<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
			    								<option value="Laki-Laki">Laki-Laki</option>
			    								<option value="Perempuan">Perempuan</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Golongan Darah</label>
			    						<div class="col-md-10">
			    							<select name="golongan_darah" id="golongan_darah" class="form-control">
			    								<option value="A">A</option>
			    								<option value="B">B</option>
			    								<option value="O">O</option>
			    								<option value="AB">AB</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Agama</label>
			    						<div class="col-md-10">
			    							<select name="agama" id="agama" class="form-control">
			    								<option value="Islam">Islam</option>
			    								<option value="Kristen">Kristen</option>
			    								<option value="Hindu">Hindu</option>
			    								<option value="Budha">Budha</option>
			    								<option value="Konghucu">Konghucu</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Pendidikan Terakhir</label>
			    						<div class="col-md-10">
			    							<select name="pendidikan" id="pendidikan" class="form-control">
			    								<option value="TK">TK</option>
			    								<option value="SD">SD/MI Sederajat</option>
			    								<option value="SMP">SMP/MTs Sederajat</option>
			    								<option value="SMA">SMA/MA/SMK Sederajat</option>
			    								<option value="Sarjana">Sarjana</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Pekerjaan</label>
			    						<div class="col-md-10">
			    							<input type="text" name="pekerjaan" id="pekerjaan" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Status Perkawinan</label>
			    						<div class="col-md-10">
			    							<select name="status_perkawinan" id="status_perkawinan" class="form-control">
			    								<option value="Belum Kawin">Belum Kawin</option>
			    								<option value="Kawin">Kawin</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Status Keluarga</label>
			    						<div class="col-md-10">
			    							<select name="status_keluarga" id="status_perkawinan" class="form-control">
			    								<option value="Istri">Istri</option>
			    								<option value="Anak">Anak</option>
			    								<option value="Kepala Keluarga">Kepala Keluarga</option>
			    							</select>
			    						</div>
			    					</div>
			    				</div>
			    				<div class="panel-footer" align="right">
			    					<a href="index.php"><button class="btn btn-danger"><i class="fa fa-times"></i> Batal</button></a>
			    					<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
			    				</div>
			    			</form>
			    		</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>		
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>