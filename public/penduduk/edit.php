<!DOCTYPE html>
<html lang="en">
<!-- Load header -->

<?php
require '../../models/PendudukModel.php';
require '../../models/KartuKeluargaModel.php';
$conn = new model_penduduk();
$kk = new model_kartu_keluarga();
?>

<?php require_once(__DIR__ . "/../layouts/header.php");?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Penduduk <small>| Edit Penduduk</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Penduduk</a></li>
					<li class="active">Edit Penduduk</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-primary">
			    			<div class="panel-heading">
			    				<h3 class="panel-title">Data Penduduk</h3>
			    			</div>

			    			<?php
	                   			$tampil = $conn->read_edit_penduduk($_GET['nik']);
				                while($fetch = $tampil->fetch_array())
				                { 
			                ?>

			    			<form role="form" method="POST" action="../../controllers/PendudukController.php?action=edit_penduduk" class="form-horizontal" enctype="multipart/form-data">


			    				<div class="panel-body">
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nomor KK</label>
			    						<div class="col-md-10">
			    							<select name="nomor_kk" id="nomor_kk" class="form-control">
			    							<?php
					                   			$tampil = $kk->read_kk();
								                while($kartukeluarga = $tampil->fetch_array())
								                { 
							                ?>
			    								<option value="<?= $kartukeluarga['nomer_kk']?>" <?= ($fetch['nomor_kk'] == $kartukeluarga['nomer_kk']) ? "selected" : ""?>><?= $kartukeluarga['nomer_kk']?> - <?= $kartukeluarga['kepala_keluarga']?></option>
			    							<?php }?>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nomor NIK</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nik" id="nik" class="form-control" value="<?php echo $fetch['nik'];?>">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nama</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" value="<?php echo $fetch['nama_lengkap'];?>">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Alamat</label>
			    						<div class="col-md-10">
			    							<textarea name="alamat" id="alamat" cols="30" rows="5" class="form-control"><?php echo $fetch['alamat'];?></textarea>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tempat Lahir</label>
			    						<div class="col-md-10">
			    							<input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" value="<?php echo $fetch['tempat_lahir'];?>">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tanggal Lahir</label>
			    						<div class="col-md-10">
			    							<input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" value="<?php echo $fetch['tanggal_lahir'];?>">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Kewarganegaraan</label>
			    						<div class="col-md-10">
			    							<input type="text" name="kewarganegaraan" id="kewarganegaraan" class="form-control" value="<?php echo $fetch['kewarganegaraan'];?>">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Jenis Kelamin</label>
			    						<div class="col-md-10">
			    							<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
			    								<option value="Laki-Laki" <?= ($fetch['jenis_kelamin'] == 'Laki-Laki') ? "selected" : ""?>>Laki-Laki</option>
			    								<option value="Perempuan" <?= ($fetch['jenis_kelamin'] == 'Perempuan') ? "selected" : ""?>>Perempuan</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Golongan Darah</label>
			    						<div class="col-md-10">
			    							<select name="golongan_darah" id="golongan_darah" class="form-control">
			    								<option value="A" <?= ($fetch['golonga_darah'] == 'A') ? "selected" : ""?>>A</option>
			    								<option value="B" <?= ($fetch['golonga_darah'] == 'B') ? "selected" : ""?>>B</option>
			    								<option value="O" <?= ($fetch['golonga_darah'] == 'O') ? "selected" : ""?>>O</option>
			    								<option value="AB" <?= ($fetch['golonga_darah'] == 'AB') ? "selected" : ""?>>AB</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Agama</label>
			    						<div class="col-md-10">
			    							<select name="agama" id="agama" class="form-control">
			    								<option value="Islam" <?= ($fetch['agama'] == 'Islam') ? "selected" : ""?>>Islam</option>
			    								<option value="Kristen" <?= ($fetch['agama'] == 'Kristen') ? "selected" : ""?>>Kristen</option>
			    								<option value="Hindu" <?= ($fetch['agama'] == 'Hindu') ? "selected" : ""?>>Hindu</option>
			    								<option value="Budha" <?= ($fetch['agama'] == 'Budha') ? "selected" : ""?>>Budha</option>
			    								<option value="Konghucu" <?= ($fetch['agama'] == 'Konghucu') ? "selected" : ""?>>Konghucu</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Pendidikan Terakhir</label>
			    						<div class="col-md-10">
			    							<select name="pendidikan" id="pendidikan" class="form-control">
			    								<option value="TK" <?= ($fetch['pendidikan'] == 'TK') ? "selected" : ""?>>TK</option>
			    								<option value="SD" <?= ($fetch['pendidikan'] == 'SD') ? "selected" : ""?>>SD/MI Sederajat</option>
			    								<option value="SMP" <?= ($fetch['pendidikan'] == 'SMP') ? "selected" : ""?>>SMP/MTs Sederajat</option>
			    								<option value="SMA" <?= ($fetch['pendidikan'] == 'SMA') ? "selected" : ""?>>SMA/MA/SMK Sederajat</option>
			    								<option value="Sarjana" <?= ($fetch['pendidikan'] == 'Sarjana') ? "selected" : ""?>>Sarjana</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Pekerjaan</label>
			    						<div class="col-md-10">
			    							<input type="text" name="pekerjaan" id="pekerjaan" value="<?php echo $fetch['pekerjaan'];?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Status Perkawinan</label>
			    						<div class="col-md-10">
			    							<select name="status_perkawinan" id="status_perkawinan" class="form-control">
			    								<option value="Belum Kawin" <?= ($fetch['status_perkawinan'] == 'Belum Kawin') ? "selected" : ""?>>Belum Kawin</option>
			    								<option value="Kawin" <?= ($fetch['status_perkawinan'] == 'Kawin') ? "selected" : ""?>>Kawin</option>
			    							</select>
			    						</div>
			    					</div>
			    					<input type="hidden" name="id_data_penduduk" value="<?= $fetch['id_data_penduduk'];?>">
			    				</div>
			    				<?php
				    				}
				    			?>
			    				<div class="panel-footer" align="right">
			    					<a href="index.php"><button class="btn btn-danger"><i class="fa fa-times"></i> Batal</button></a>
			    					<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
			    				</div>
			    			</form>
			    		</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>		
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>