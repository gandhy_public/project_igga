<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/PerpindahanModel.php';
$conn = new model_perpindahan();
?>
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Data</b>Kependudukan</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="../../controllers/AuthController.php?action=login" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name='username' placeholder="Username">
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name='password' placeholder="Password">
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>

</body>
</html>