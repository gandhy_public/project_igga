<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/DesaModel.php';
$conn = new model_desa();
?>
<!-- KEPALA -->
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<!-- SAMPING -->
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- UTAMA -->
		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Desa
					<small>| Daftar Desa</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Desa</a></li>
					<li class="active">Daftar Desa</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
				    		<div class="panel-body">
				    			<table class="table table-striped table-hover">
				    				<thead>
				    					<tr>
				    						<th>No</th>
				    						<th>Nama Desa</th>
				    					</tr>
				    				</thead>
				    				<!-- MEMUNCULKAN DI TABLE -->
				    				<tbody>
				    					<?php
				    					$nomor = 1;
				    					$read_desa = $conn->read_desa();
				    					while($fetch = $read_desa->fetch_array()){
				    						?>
				    					<tr>
				    							<td><?php echo $nomor++ ?>.</td>
				    							<td><?php echo $fetch['nama_desa']?></td>
				    							<td>
				    							<!--HAPUS DATA-->
				    							<a class="btn btn-danger btn-sm" href="../../controllers/DesaController.php?id_desa=<?php echo $fetch['id_desa']?>&action=hapus_desa"><i class="fa fa-trash"></i></a>
				    							<!--EDIT DATA-->
				    							<a class="btn btn-warning btn-sm" href="edit.php?id_desa=<?php echo $fetch['id_desa'];?>"><i class="fa fa-pencil-square-o"></i></a>
				    					</tr>
				    				<?php
				    				}
				    				?>
				    				</tbody>

				    				
				    			</table>
				    			<div class="panel-footer" align="right">
			    				<a class="btn btn-primary" href="tambah.php"><i class="fa fa-plus"></i> Tambah Data</a>
			    				</div>
				    		</div>
				    	</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- BAWAH -->

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>

	</div>

<!-- Load javascripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>