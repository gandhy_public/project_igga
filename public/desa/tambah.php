<!DOCTYPE html>
<html lang="en">
<!-- Load header -->

<?php
require '../../models/DesaModel.php';
$conn = new model_desa();
?>

<?php require_once(__DIR__ . "/../layouts/header.php");?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Kematian <small>| Tambah Kematian</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data kematian</a></li>
					<li class="active">Tambah Kematian</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-primary">
			    			<div class="panel-heading">
			    				<h3 class="panel-title">Data Kematian Baru</h3>
			    			</div>


			    			<form role="form" method="POST" action="../../controllers/DesaController.php?action=tambah_desa" class="form-horizontal">


			    				<div class="panel-body">

			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">ID Desa</label>
			    						<div class="col-md-10">
			    							<input type="text" name="id_desa" id="id_desa" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nama Desa</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nama_desa" id="nama_desa" class="form-control">
			    						</div>
			    					</div>
			    				</div>
			    				
			    				<div class="panel-footer" align="right">
			    					<button class="btn btn-danger"><i class="fa fa-times"></i> Batal</button>
			    					<button class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
			    				</div>
			    			</form>
			    		</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>		
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>