<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/KelahiranModel.php';
$conn = new model_kelahiran();
?>
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Kelahiran
					<small>| Daftar Kelahiran</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Kelahiran</a></li>
					<li class="active">Daftar Kelahiran</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
				    		<div class="panel-body">
				    			<table class="table table-striped table-hover">
				    				<thead>
				    					<tr>
				    						<th>Nomor</th>
				    						<th>NIK</th>
				    						<th>Nama</th>
				    						<th>Tanggal Lahir</th>
				    						<th>Action</th>
				    					</tr>
				    				</thead>
				    				<tbody>
				    					<?php
				    					$nomor = 1;
				    					$read_kelahiran = $conn->read_kelahiran();
				    					while($fetch = $read_kelahiran->fetch_array()){
				    						?>
				    					<tr>
				    						<td><?php echo $nomor++ ?>.</td>
			    							<td><?php echo $fetch['nomor_nik']?></td>
			    							<td><?php echo $fetch['nama']?></td>
			    							<td><?php echo $fetch['tanggal_lahir']?></td>
				    						<td>
				    							<a href="../../controllers/KelahiranController.php?id=<?php echo $fetch['id_data_kelahiran']?>&action=hapus_kelahiran"><button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></a>
				    							<a href="edit.php?id=<?php echo $fetch['id_data_kelahiran'];?>"><button class="btn btn-warning btn-sm"><i class="fa fa-pencil-square-o"></i></button></a>
				    						</td>
				    					</tr>
				    					<?php }?>
				    				</tbody>
				    			</table>
				    		</div>
				    	</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>