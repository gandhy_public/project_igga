<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/KartuKeluargaModel.php';
require '../../models/KelahiranModel.php';
$conn = new model_kartu_keluarga();
$conn2 = new model_kelahiran();
?>
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Kelahiran
					<small>| Edit Kelahiran</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Kelahiran</a></li>
					<li class="active">Edit Kelahiran</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-primary">
			    			<div class="panel-heading">
			    				<h3 class="panel-title">Edit Data Kelahiran</h3>
			    			</div>

			    			<?php
	                   			$tampil_kelahiran = $conn2->read_edit_kelahiran($_GET['id']);
				                while($kelahiran = $tampil_kelahiran->fetch_array())
				                { 
			                ?>

			    			<form action="../../controllers/KelahiranController.php?action=edit_kelahiran" method="POST" class="form-horizontal">
			    				<div class="panel-body">
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nomor KK</label>
			    						<div class="col-md-10">
			    							<select name="nomor_kk" id="nomor_kk" class="form-control">
			    							<?php
					                   			$tampil = $conn->read_kk();
								                while($fetch = $tampil->fetch_array())
								                { 
							                ?>
			    								<option value="<?= $fetch['nomer_kk']?>" <?= ($fetch['nomer_kk'] == $kelahiran['nomor_kk']) ? "selected" : "" ?>><?= $fetch['nomer_kk']?> - <?= $fetch['kepala_keluarga']?></option>
			    							<?php }?>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="panel-body">
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nomor NIK</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nomor_nik" id="nomor_nik" value="<?= $kelahiran['nomor_nik']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nama Lengkap</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nama_lengkap" id="nama_lengkap" value="<?= $kelahiran['nama']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Jenis Kelamin</label>
			    						<div class="col-md-10">
			    							<select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
			    								<option value="Laki-Laki" <?= ($kelahiran['jenis_kelamin'] == 'Laki-Laki') ? "selected" : ""?>>Laki-Laki</option>
			    								<option value="Perempuan" <?= ($kelahiran['jenis_kelamin'] == 'Perempuan') ? "selected" : ""?>>Perempuan</option>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tempat Lahir</label>
			    						<div class="col-md-10">
			    							<input type="text" name="tempat_lahir" id="tempat_lahir" value="<?= $kelahiran['tempat_lahir']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Hari Lahir</label>
			    						<div class="col-md-10">
			    							<input type="text" name="hari_lahir" id="hari_lahir" value="<?= $kelahiran['hari_lahir']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tanggal Lahir</label>
			    						<div class="col-md-10">
			    							<input type="date" name="tanggal_lahir" id="tanggal_lahir" value="<?= $kelahiran['tanggal_lahir']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Jam Lahir</label>
			    						<div class="col-md-10">
			    							<input type="time" name="jam_lahir" id="jam_lahir" value="<?= $kelahiran['jam_lahir']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tanggal Pengajuan Lahir</label>
			    						<div class="col-md-10">
			    							<input type="date" name="tanggal_pengajuan_lahir" id="tanggal_pengajuan_lahir" value="<?= $kelahiran['tanggal_pengajuan_lahir']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nama Ibu</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nama_ibu" id="nama_ibu" value="<?= $kelahiran['nama_ibu']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Alamat Ibu</label>
			    						<div class="col-md-10">
			    							<textarea name="alamat_ibu" id="alamat_ibu" cols="30" rows="5" class="form-control"><?= $kelahiran['alamat_ibu'] ?></textarea>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Umur Ibu</label>
			    						<div class="col-md-10">
			    							<input type="text" name="umur_ibu" id="umur_ibu" value="<?= $kelahiran['umur_ibu']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nama Ayah</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nama_ayah" id="nama_ayah" value="<?= $kelahiran['nama_ayah']?>" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Alamat Ayah</label>
			    						<div class="col-md-10">
			    							<textarea name="alamat_ayah" id="alamat_ayah" cols="30" rows="5" class="form-control"><?= $kelahiran['alamat_ayah']?></textarea>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Umur Ayah</label>
			    						<div class="col-md-10">
			    							<input type="text" name="umur_ayah" id="umur_ayah" value="<?= $kelahiran['umur_ayah']?>" class="form-control">
			    						</div>
			    					</div>
			    					<input type="hidden" name="id_data_kelahiran" value="<?= $kelahiran['id_data_kelahiran']?>">
			    				<div class="panel-footer" align="right">
			    					<a href="index.php"><button class="btn btn-danger"><i class="fa fa-times"></i> Batal</button></a>
			    					<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
			    				</div>
			    			</form>

			    			<?php }?>
			    		</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>