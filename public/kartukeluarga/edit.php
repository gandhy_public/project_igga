<!DOCTYPE html>
<html lang="en">
<!-- Load header -->

<?php
require '../../models/KartuKeluargaModel.php';
$conn = new model_kartu_keluarga();
?>

<?php require_once(__DIR__ . "/../layouts/header.php");?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Kartu Keluarga <small>| Edit Data Kartu Keluarga</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Kartu Keluarga</a></li>
					<li class="active">Edit Kartu Keluarga</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-primary">
			    			<div class="panel-heading">
			    				<h3 class="panel-title">Data Kartu Keluarga</h3>
			    			</div>

			    			<?php
	                   			$tampil = $conn->read_edit_kk($_GET['nomor_kk']);
				                while($fetch = $tampil->fetch_array())
				                { 
			                ?>

			    			<form role="form" method="POST" action="../../controllers/KartuKeluargaController.php?action=edit_kk" class="form-horizontal" enctype="multipart/form-data">


			    				<div class="panel-body">

			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nomor KK</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nomor_kk" id="nomor_kk" class="form-control" value="<?php echo $fetch['nomer_kk'];?>" readonly>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Nama Kepala Keluarga</label>
			    						<div class="col-md-10">
			    							<input type="text" name="nama_kk" id="nama_kk" class="form-control" value="<?php echo $fetch['kepala_keluarga'];?>">
			    						</div>
			    					</div>
			    				</div>
			    				<?php
				    				}
				    			?>
			    				<div class="panel-footer" align="right">
			    					<a href="index.php"><button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Batal</button></a>
			    					<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
			    				</div>
			    			</form>
			    		</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>		
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>