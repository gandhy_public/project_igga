<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/KartuKeluargaModel.php';
$conn = new model_kartu_keluarga();
?>
<!-- KEPALA -->
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<!-- SAMPING -->
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- UTAMA -->
		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Anggota Keluarga
					<small>| Daftar Anggota Keluarga</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Anggota Keluarga</a></li>
					<li class="active">Daftar Anggota Keluarga</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
				    		<div class="panel-body">
				    			<table class="table table-striped table-hover">
				    				<thead>
				    					<tr>
				    						<th>No</th>
				    						<th>Nomor Induk Keluarga</th>
				    						<th>Nama Lengkap</th>
				    						<th>Status Keluarga</th>
				    					</tr>
				    				</thead>
				    				<!-- MEMUNCULKAN DI TABLE -->
				    				<tbody>
				    					<?php
				    					$nokk = $_GET['nomor_kk'];
				    					$nomor = 1;
				    					$read_kk = $conn->detail_kk($nokk);
				    					while($fetch = $read_kk->fetch_array()){
				    						?>
				    					<tr>
				    							<td><?php echo $nomor++ ?>.</td>
				    							<td><?php echo $fetch['nik']?></td>
				    							<td><?php echo $fetch['nama_lengkap']?></td>
				    							<td><?php echo $fetch['status_keluarga']?></td>
				    							<td>
				    							
				    							
				    					</tr>
				    				<?php
				    				}
				    				?>
				    				</tbody>

				    				
				    			</table>
				    		</div>
				    	</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- BAWAH -->

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>

	</div>

<!-- Load javascripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>