<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/KartuKeluargaModel.php';
$conn = new model_kartu_keluarga();
?>
<!-- KEPALA -->
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<!-- SAMPING -->
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- UTAMA -->
		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Kartu Keluarga
					<small>| Daftar Kartu Keluarga</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Kartu Keluarga</a></li>
					<li class="active">Daftar Kartu Keluarga</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
				    		<div class="panel-body">
				    			<table class="table table-striped table-hover">
				    				<thead>
				    					<tr>
				    						<th>No</th>
				    						<th>Nomor Kartu Keluarga</th>
				    						<th>Nama Kepala Keluarga</th>
				    					</tr>
				    				</thead>
				    				<!-- MEMUNCULKAN DI TABLE -->
				    				<tbody>
				    					<?php
				    					$nomor = 1;
				    					$read_kk = $conn->read_kk();
				    					while($fetch = $read_kk->fetch_array()){
				    						?>
				    					<tr>
				    							<td><?php echo $nomor++ ?>.</td>
				    							<td><?php echo $fetch['nomer_kk']?></td>
				    							<td><?php echo $fetch['kepala_keluarga']?></td>
				    							<td>
				    							<!--HAPUS DATA-->
				    							<?php if($_SESSION['role'] == 'desa') : ?>
				    								<a class="btn btn-danger btn-sm" href="../../controllers/KartuKeluargaController.php?nomor_kk=<?php echo $fetch['nomer_kk']?>&action=hapus_kk"><i class="fa fa-trash"></i></a>
					    							<!--EDIT DATA-->
					    							<a class="btn btn-warning btn-sm" href="detail.php?nomor_kk=<?php echo $fetch['nomer_kk'];?>"><i class="fa fa-pencil-square-o"></i></a>
					    							<a class="btn btn-default btn-sm" href="../penduduk/tambah-penduduk.php?nomor_kk=<?php echo $fetch['nomer_kk'];?>"><i class="fa fa-plus"></i></a>
					    						<?php endif; ?>
				    					</tr>
				    				<?php
				    				}
				    				?>
				    				</tbody>

				    				
				    			</table>
				    			<div class="panel-footer" align="right">
			    				<a class="btn btn-primary" href="tambah-kk.php"><i class="fa fa-plus"></i> Tambah Data</a>
			    				</div>
				    		</div>
				    	</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- BAWAH -->

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>

	</div>

<!-- Load javascripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>