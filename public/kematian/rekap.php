<!DOCTYPE html>
<html lang="en">

<?php
require '../../models/KematianModel.php';
$conn = new model_kematian();
?>
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		
		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Rekap
					<small>| Kematian</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Rekap</a></li>
					<li class="active">Kematian</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
				    		<div class="panel-body">
				    			<table class="table table-striped table-hover">
				    				<thead>
				    					<tr>
				    						<th>Nomor</th>
				    						<th>Bulan</th>
				    						<th>Jumlah Kelahiran</th>
				    					</tr>
				    				</thead>
				    				<tbody>
				    					<?php
				    					$nomor = 1;
				    					$read_kelahiran = $conn->rekap_kematian();
				    					while($fetch = $read_kelahiran->fetch_array()){
				    						?>
				    					<tr>
				    						<td><?php echo $nomor++ ?>.</td>
			    							<td><?php echo $fetch['bulan']?></td>
			    							<td><?php echo $fetch['jumlah']?></td>
				    					</tr>
				    					<?php }?>
				    				</tbody>
				    			</table>
				    		</div>
				    	</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>