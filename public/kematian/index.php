<!DOCTYPE html>
<html lang="en">

<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php"); ?>

<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require '../../models/KematianModel.php';
$conn = new model_kematian();
?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<!-- Content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Kematian
					<small>| Daftar Kematian</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data Kematian</a></li>
					<li class="active">Daftar Kematian</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-default">
				    		<div class="panel-body">
				    			<table class="table table-striped table-hover">
				    				<thead>
				    					<tr>
				    						<th>NIK</th>
				    						<th>Nama</th>
				    						<th>Tempat Meninggal</th>
				    						<th>Action</th>
				    					</tr>
				    				</thead>
				    				<tbody>
				    					<?php
				    					$read_kematian = $conn->read_kematian();
				    					while($row = $read_kematian->fetch_array()) {
				    					?>
				    					<tr>
				    						<td><?= $row['data_penduduk_nik'] ?></td>
				    						<td><?= $row['nama_lengkap'] ?></td>
				    						<td><?= $row['tempat_meninggal'] ?></td>
				    						<td>
				    							<a class="btn btn-danger btn-sm" href="../../controllers/KematianController.php?id=<?php echo $row['id_data_kematian']?>&action=hapus_kematian"><i class="fa fa-trash"></i></a>
				    							<!-- <a class="btn btn-warning btn-sm" href="edit.php?id=<?php //echo $row['id_data_kematian'];?>"><i class="fa fa-pencil-square-o"></i></a> -->
				    						</td>
				    					</tr>
				    					<?php } ?>
				    				</tbody>
				    			</table>
				    		</div>
				    	</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>

	</div>

<!-- Load javascripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>