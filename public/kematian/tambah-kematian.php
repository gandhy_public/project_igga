<!DOCTYPE html>
<html lang="en">
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php");?>

<?php
require '../../models/PendudukModel.php';
$conn = new model_penduduk();
?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Data Kematian <small>| Tambah Kematian</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Data kematian</a></li>
					<li class="active">Tambah Kematian</li>
				</ol>
		    </section>

		    <!-- Content -->
		    <section class="content">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="panel panel-primary">
			    			<div class="panel-heading">
			    				<h3 class="panel-title">Data Kematian Baru</h3>
			    			</div>
			    			<form action="../../controllers/KematianController.php?action=tambah_kematian" class="form-horizontal" method="POST">
			    				<div class="panel-body">
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">NIK Penduduk</label>
			    						<div class="col-md-10">
			    							<select name="penduduk_nik" id="" class="form-control">
			    								<?php
					                   			$tampil = $conn->read_penduduk();
								                while($fetch = $tampil->fetch_array())
								                {
							                ?>
			    								<option value="<?= $fetch['nik'] . ';' . $fetch['id_data_penduduk']?>"><?= $fetch['nik']?> - <?= $fetch['nama_lengkap']?></option>
			    							<?php }?>
			    							</select>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tempat Meninggal</label>
			    						<div class="col-md-10">
			    							<textarea name="tempat_meninggal" id="" cols="30" rows="5" class="form-control"></textarea>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Hari Meninggal</label>
			    						<div class="col-md-10">
			    							<input type="text" name="hari_meninggal" id="" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tanggal Meninggal</label>
			    						<div class="col-md-10">
			    							<input type="date" name="tanggal_meninggal" id="" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Usia Meninggal</label>
			    						<div class="col-md-10">
			    							<input type="number" name="usia_meninggal" id="" class="form-control">
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Penyebab Kematian</label>
			    						<div class="col-md-10">
			    							<textarea name="penyebab_kematian" id="" cols="30" rows="5" class="form-control"></textarea>
			    						</div>
			    					</div>
			    					<div class="form-group">
			    						<label style="text-align: left;" for="" class="col-sm-2 control-label">Tanggal Pengajuan Meninggal</label>
			    						<div class="col-md-10">
			    							<input type="date" name="tanggal_pengajuan_meninggal" id="" class="form-control">
			    						</div>
			    					</div>
			    				</div>
			    				<div class="panel-footer" align="right">
			    					<button class="btn btn-danger"><i class="fa fa-times"></i> Batal</button>
			    					<button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Simpan</button>
			    				</div>
			    			</form>
			    		</div>
		    		</div>
		    	</div>
		    </section>
		</div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>