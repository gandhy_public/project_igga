<?php session_start(); ?>
<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SIK</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">SI KEPENDUDUKAN</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <!-- <div class="navbar-custom-menu">
            
        </div> -->
    </nav>
</header>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo __BASE_URI__ ?>assets/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>

            <div class="pull-left info">
                <p><?= (isset($_SESSION['logged']) ? $_SESSION['name'] : "guess"); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="<?php echo __BASE_URI__ ?>public">
                    <i class="fa fa-dashboard"></i>
                    <span>Beranda</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Data Kartu Keluarga</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if($_SESSION['role'] == 'desa') : ?>
                        <li><a href="<?php echo __BASE_URI__ ?>public/kartukeluarga/tambah-kk.php"><i class="fa fa-circle-o"></i> Tambah Kartu Keluarga</a></li>
                        <li><a href="<?php echo __BASE_URI__ ?>public/kartukeluarga"><i class="fa fa-circle-o"></i> Daftar Kartu Keluarga</a></li>
                    <?php else : ?>
                        <li><a href="<?php echo __BASE_URI__ ?>public/kartukeluarga"><i class="fa fa-circle-o"></i> Daftar Kartu Keluarga</a></li>
                    <?php endif; ?>
                </ul>
            </li>
        
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Data Penduduk</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if($_SESSION['role'] == 'desa') : ?>
                        <!-- <li><a href="<?php echo __BASE_URI__ ?>public/penduduk/tambah-penduduk.php"><i class="fa fa-circle-o"></i> Tambah Penduduk</a></li> -->
                        <li><a href="<?php echo __BASE_URI__ ?>public/penduduk"><i class="fa fa-circle-o"></i> Daftar Penduduk</a></li>
                    <?php else : ?>
                        <li><a href="<?php echo __BASE_URI__ ?>public/penduduk"><i class="fa fa-circle-o"></i> Daftar Penduduk</a></li>
                    <?php endif; ?>    
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-child"></i>
                    <span>Data Kelahiran</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if($_SESSION['role'] == 'desa') : ?>
                        <li><a href="<?php echo __BASE_URI__ ?>public/kelahiran/tambah-kelahiran.php"><i class="fa fa-circle-o"></i> Tambah Kelahiran</a></li>
                        <li><a href="<?php echo __BASE_URI__ ?>public/kelahiran"><i class="fa fa-circle-o"></i> Daftar Kelahiran</a></li>
                    <?php else : ?>
                        <li><a href="<?php echo __BASE_URI__ ?>public/kelahiran"><i class="fa fa-circle-o"></i> Daftar Kelahiran</a></li>
                    <?php endif; ?>    
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-times"></i>
                    <span>Data Kematian</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if($_SESSION['role'] == 'desa') : ?>
                        <li><a href="<?php echo __BASE_URI__ ?>public/kematian/tambah-kematian.php"><i class="fa fa-circle-o"></i> Tambah Kematian</a></li>
                        <li><a href="<?php echo __BASE_URI__ ?>public/kematian"><i class="fa fa-circle-o"></i> Daftar Kematian</a></li>
                    <?php else : ?>
                        <li><a href="<?php echo __BASE_URI__ ?>public/kematian"><i class="fa fa-circle-o"></i> Daftar Kematian</a></li>
                    <?php endif; ?>    
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-sign-out"></i>
                    <span>Data Perpindahan</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php if($_SESSION['role'] == 'desa') : ?>
                        <li><a href="<?php echo __BASE_URI__ ?>public/perpindahan/tambah-perpindahan.php"><i class="fa fa-circle-o"></i> Tambah Perpindahan</a></li>
                        <li><a href="<?php echo __BASE_URI__ ?>public/perpindahan"><i class="fa fa-circle-o"></i> Daftar Perpindahan</a></li>
                    <?php else : ?>
                        <li><a href="<?php echo __BASE_URI__ ?>public/perpindahan"><i class="fa fa-circle-o"></i> Daftar Perpindahan</a></li>
                    <?php endif; ?>    
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Rekapitulasi</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo __BASE_URI__ ?>public/penduduk/rekap.php"><i class="fa fa-circle-o"></i> Penduduk</a></li>
                    <li><a href="<?php echo __BASE_URI__ ?>public/kelahiran/rekap.php"><i class="fa fa-circle-o"></i> Kelahiran</a></li>
                    <li><a href="<?php echo __BASE_URI__ ?>public/perpindahan/rekap.php"><i class="fa fa-circle-o"></i> Perpindahan</a></li>

                </ul>
            </li>

            <?php if($_SESSION['logged'] == true) : ?>
            <li class="menu">
                <a href="<?php echo __BASE_URI__ ?>controllers/AuthController.php?action=logout">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                </a>
            </li>
            <?php endif;?>

            
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>