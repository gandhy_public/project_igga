<!DOCTYPE html>
<html lang="en">
<!-- Load header -->
<?php require_once(__DIR__ . "/../layouts/header.php");?>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<!-- Load sidebar -->
		<?php require_once(__DIR__ . "/../layouts/sidebar.php"); ?>

	    <!-- Content -->
	    <div class="content-wrapper">
	    	<!-- Content Header (Page header) -->
		    <section class="content-header">
				<h1>
					Rekapitulasi
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Rekapitulasi</a></li>
				</ol>
		    </section>

		    <section class="content">
		    	
		    </section>
	    </div>

		<!-- Load footer -->
		<?php require_once(__DIR__ . "/../layouts/footer.php"); ?>		
	</div>

<!-- Load scripts -->
<?php require_once(__DIR__ . "/../layouts/scripts.php"); ?>
</body>
</html>